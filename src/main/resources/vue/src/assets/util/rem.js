(function () {
  let rootEl = document.documentElement
  let getSize = function () {
    let clientWidth = rootEl.getBoundingClientRect().width
    rootEl.style.fontSize = (100 / 750) * (clientWidth >= 750 ? 750 : clientWidth) + 'px'
  }
  let timer = null
  window.addEventListener('resize', function () {
    clearTimeout(timer)
    timer = setTimeout(getSize, 300)
  })
  getSize()
})()
